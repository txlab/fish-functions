# Our fish functions

### Install
Installable with [omf](https://github.com/oh-my-fish/oh-my-fish) / [fundle](https://github.com/danhper/fundle/) - fisher [not yet](https://github.com/jorgebucaran/fisher/pull/658#issuecomment-1159772208)
```fish
omf install https://gitlab.com/txlab/fish-functions
```

via nix home-manager:
```
programs.fish = {
    enable = true;
    plugins = [
        {
            name = "txlab-functions";
            src = pkgs.fetchFromGitLab {
            owner = "txlab";
            repo = "fish-functions";
            rev = "613b2cdbce2be17ac70bfd6f179b0ba3d35f6033";
            sha256 = "/GKT3lvfr7muDvjm31kUBStMQ7BujL/lw4r3RgB4mNo=";
            };
        }
    ];
};
```

## Documentation
I started documenting _some_ commands are in [`docs/`](./docs/)

TODO: auto-generate overview from --description


## Dev setup
Option 1 (dev only)
```fish
git clone git@gitlab.com:txlab/fish-functions.git
cd fish-functions/
rm -rf $OMF_PATH/pkg/fish-functions || true
ln -s $PWD $OMF_PATH/pkg/fish-functions
```

Option 2 (dev trumps latest)
add the following an auto sourced file such as ".config/fish/conf.d/tx-fish-functions.fish" 
set fish_function_path $fish_function_path[1] $the_dir_where_you_cloned_the_repo/functions $fish_function_path[2..-1]

eg via nix home-manager:
```
  home.file = {
    # dev setup for fish-functions
    ".config/fish/conf.d/tx-fish-functions.fish" = {
      text = ''
        set fish_function_path $fish_function_path[1] ~/Dev/fish-functions/functions $fish_function_path[2..-1] 
      '';
    };
  };
```
### ~~Best~~ Good practices
- for functions with many options, consider [`fish_opt`](https://fishshell.com/docs/current/cmds/fish_opt.html#cmd-fish-opt)
- Use [snippets](.vscode/fish.code-snippets)
```fish
function func-name -d "Short description"
    argparse 'h/help' 'o/output=' -- $argv; or return
    set -q _flag_h || test (count $argv) -eq 0 && begin
        echo 'Usage: func-name [-o/--output FILE] repo [filter]'
        return
    end
    set url (arg_require $argv[1]) || return
    set filter $argv[2]
    test -n "$filter"; or set filter 'default value'

    echo url=$url filter=$filter out=$_flag_o

    command-shall-not-fail; or echo 'download error' >&2; and return
end
```

TODO: [tests?](https://github.com/terlar/fish-tank)