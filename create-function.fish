#!/usr/bin/env fish

set name $argv[1]
set path functions/$name.fish

cp _function_template.fish $path
and sd NAME $name $path
and e $path
