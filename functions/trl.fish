#!/usr/bin/env fish
function trl --wraps='tree' --description 'tree with details, like ls -l'
  tr -pug -h $argv
end
