#!/usr/bin/env fish

function which-ssh -a ip -d 'which of my SSH servers hosts $ip'
    set tmp (mktemp)
    set ssh_hosts (rg 'HostName (.+)' ~/.ssh/config -or '$1' | sort | uniq)
    echo Checking (count $ssh_hosts) "ssh hosts"
    for host in $ssh_hosts
        if dig +short $host | rg $ip
            echo Match: $host
            return
        end
    end
    echo "Not found: $ip"
end
