#!/usr/bin/env fish
function sudof -d "Run sudo command but with fish functions available"
    # this would not escape arguments correctly:
    #sudo fish -C "set fish_function_path $fish_function_path" -c "$argv"

    # HACK: weird escaping situation for args with spacecs
    # similar to https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
    # I solved it like this:
    #   $ string join ' ' (string escape -- 1 '2 3')
    #   => 1 '2 3'

    sudo fish -C "set fish_function_path $fish_function_path" -c "$(string join -- ' ' (string escape -- $argv))"

end
