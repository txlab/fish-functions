#!/usr/bin/env fish
function sued --wraps='sudo ed' --wraps='sudo $EDITOR' --description 'alias for calling $EDITOR with sudo'
  if test -n "$SU_EDITOR"
    sudo $SU_EDITOR $argv
  else
    sudo $EDITOR $argv
  end
end
