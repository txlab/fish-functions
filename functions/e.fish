#!/usr/bin/env fish
function e --wraps='$EDITOR' --description 'alias for calling $EDITOR'
    if set -q BG_EDITOR
        eval $BG_EDITOR (string escape -- $argv)
    else if set -q VISUAL
        eval $VISUAL (string escape -- $argv)
    else
        eval $EDITOR (string escape -- $argv)
    end
end
