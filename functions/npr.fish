function npr --wraps='nix run' --description 'Run nix package $argv[1] with arguments $argv[2..]'
    printrun nix run (nix-expand-simple-refs $argv[1]) -- $argv[2..]
end
