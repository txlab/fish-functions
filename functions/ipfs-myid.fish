#!/usr/bin/env fish
# Defined in - @ line 1
function ipfs-myid --wraps='ipfs id | jq -r .ID' --description 'alias ipfs-myid ipfs id | jq -r .ID'
  ipfs id | jq -r .ID $argv;
end
