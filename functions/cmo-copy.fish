#!/usr/bin/env fish

function cmo-copy -d "Copy mod&own from REFERENCE to ...TARGETS" -a reference
    set -q _flag_h || test (count $argv) -lt 2 && begin
        echo 'Usage: cmo-copy REFERENCE ...TARGETS'
        return
    end
    sudo chmod --reference $reference $argv[2..]; or echo 'chmod error' >&2; and return 1
    sudo chown --reference $reference $argv[2..]; or echo 'chown error' >&2; and return 1
end
