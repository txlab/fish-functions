#!/usr/bin/env fish

function gitlab-ci-test --description 'test gitlab-runner locally (docker executor)' --arg jobname
    printrun npr gitlab-runner -l debug exec docker $jobname
end
