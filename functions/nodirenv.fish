# Needs ~/.config/direnv/lib/00-disable.sh with content:
#   ${DIRENV_DISABLE:+exit}
#
# see: https://github.com/direnv/direnv/issues/550#issuecomment-624403455

function nodirenv --wraps='env -i DIRENV_DISABLE=1 HOME=$HOME TERM=$TERM bash -lc' --description 'alias mete env -i DIRENV_DISABLE=1 HOME=$HOME TERM=$TERM bash -lc'
    env -i DIRENV_DISABLE=1 HOME=$HOME TERM=$TERM bash -lc "$argv"
end
