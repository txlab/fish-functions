function ns --wraps='nix shell' --description 'alias ns nix shell nixpkgs#$argv[1] $argv[2..]'
    # printrun nix shell (nix-expand-simple-refs $argv[1]) $argv[2..] # not sure why I preferred this..?
    printrun nix shell (nix-expand-simple-refs $argv)
end
