#!/usr/bin/env fish
# Defined in - @ line 1
function yadd --wraps='yarn add' --description 'alias yadd yarn add'
  yarn add $argv;
end
