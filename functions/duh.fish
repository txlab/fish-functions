#!/usr/bin/env fish
# Overwritten from /usr/share/fish/functions/la.fish
# we don't like the -A but want -a, to see '.'
# macOS has broke-ass BSD utils https://betterprogramming.pub/how-to-make-macos-command-utilities-compatible-with-gnu-core-utilities-87889b266f4b
function duh --wraps=du --description 'Check disk usage with du, sorted and formated humanely (add -d2 to limit depth to 2... or any other du options for  that matter)'
    du -h $argv | sort -h
end
