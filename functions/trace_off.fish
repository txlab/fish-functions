function trace_off --description 'disable fish_trace'
    set -g -e fish_trace
end
