function nprs --wraps='nix run' --description 'Run nix package $argv[1] with sudo and arguments $argv[2..]'
    printrun sudo nix run (nix-expand-simple-refs $argv[1]) -- $argv[2..]
end
