#!/usr/bin/env fish
function apti --wraps='sudo apt install' --description 'alias for: sudo apt install'
    sudo apt install $argv
end
