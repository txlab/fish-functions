#!/usr/bin/env fish
function tra --wraps=tree --description 'tree -a'
  trl -a $argv
end
