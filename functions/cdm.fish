#!/usr/bin/env fish
function cdm --description 'alias cdm mkdir -p $argv; and cd'
	mkdir -p $argv;
	cd $argv[-1]
end
