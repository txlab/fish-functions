#!/usr/bin/env fish

function get-ext -d "DESCRIPTION"
    set input
    if test (count $argv) -eq 0
        read -l input
        echo READ: $in
        set -g input $in
        echo READ: $input
    end
    echo READ: $input
    
    printrun basename $input | string split -r -m 1 . | tail -n 1
end
