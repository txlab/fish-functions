#!/usr/bin/env fish
function copyshell --description 'Copy PWD to tempdir and open a shell inside it'
  argparse -i 'r/rsync-opts=?' -- $argv; or return
  set tmpdir (mktemp -d); or return
  printrun rsync -a $_flag_r $PWD/ $tmpdir/; or return
  fish -C "cd $tmpdir" $argv; or return $status
end
