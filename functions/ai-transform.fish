function ai-transform -d "Apply AI transformation"
    argparse h/help 'i/input=' 'f/file=' 'o/output=' -- $argv; or return

    if set -q _flag_h || not set -q _flag_file && test (count $argv) -eq 0
        echo 'Usage: ai-transform [-i/--input=FILE] [-f/--file] [-o/--output=FILE] [prompt]'
        return
    end

    if set -q _flag_file
        set prompt (cat $_flag_file)
    else if test (count $argv) -ge 1
        set prompt $argv[1]
    else
        echo 'Error: You must provide a prompt either as a positional argument or with -f/--file'
        return
    end

    if set -q _flag_input
        set input $_flag_input
    else
        set input (mktemp)
        echo Reading input from stdin...
        cat >$input
    end

    echo Transforming "$input" # using prompt: $prompt
    echo
    set temp_output (mktemp)
    begin
        echo $prompt
        echo
        cat $input; or return 1
    end | chatgpt -n | tee $temp_output
    or return $status # FIX: use status of chatgpt command (or any in the pipe)

    # Extracting the part between the markdown code fences
    set output (mktemp)
    set fence_lines (rg -n '^```' $temp_output | cut -d: -f1)
    set fence_count (count $fence_lines)
    if test $fence_count -eq 0
        # No fences, output the original content
        set output $temp_output
    else if test $fence_count -eq 2
        # Two fences, extract lines between the fences
        set start (math $fence_lines[1] + 1)
        set end (math $fence_lines[2] - 1)
        set output (mktemp)
        sed -n $start","$end"p" $temp_output >$output
    else
        echo 'Invalid fence count: $fence_count - not sure how to extract code from $temp_output'
        return 1
    end

    echo
    difft $input $output
    echo

    if set -q _flag_output
        cat $output >$_flag_output
        echo "Saved output to $_flag_output"
    else if set -q _flag_input
        read -l -P "Do you want to overwrite the input file with the output? (Y/n): " choice
        or set -l choice n # Default to 'no' if read was interrupted
        if test "$choice" = Y -o "$choice" = y -o "$choice" = ""
            set bkp (mktemp)
            cat $input >$bkp
            cat $output >$input
            echo "Applied changes - backup of original: $bkp"
        else
            echo "Did not apply: $output"
        end
    else
        echo "Output file: $output"
    end
end
