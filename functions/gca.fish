function gca --wraps='gc -a' --description 'alias gca gc -a'
  git add -A
  and gc -a $argv; 
end
