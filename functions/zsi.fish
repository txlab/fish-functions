#!/usr/bin/env fish

function zsi -d "z-jump into session dir"
    zi ~/dev/sessions "$argv"
end
