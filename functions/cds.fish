#!/usr/bin/env fish

function cds -d 'go in a session directory'
    if test -n "$FISH_CDSESSION_BASEDIR"
        set basedir $FISH_CDSESSION_BASEDIR
    else
        set basedir $HOME/dev/sessions
    end
    set dir "$basedir/"(date -Is \
        | string replace -r '\+[0-9]{2}:[0-9]{2}$' '' \
        | string replace -a ':' '-') # colons make problems - https://stackoverflow.com/a/29213487
    if test -n "$argv[1]"
        set dir $dir'_'(echo "$argv"| string replace -a -r '\s+' '_')
    end

    mkdir -p $dir; or return 1
    printrun cd $dir; or return 1

    # TODO: register hook &&/|| save atuin
    # TODO: dvnv init ?
end
