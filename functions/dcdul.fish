function dcdul --wraps='dc down; and dcul' --description 'alias dcdul dc down; and dcul'
  dc down; and dcul $argv; 
end
