#!/usr/bin/env fish

function which-trace -d "realpath of which"
    realpath-trace (which $argv[1])
end
