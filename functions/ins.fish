#!/usr/bin/env fish
function ins --description 'Install package or deb/rpm file (download if url)'
    set file (arg_require $argv[1]) || return

    # URL ?
    if string match '*//*' $file 
        echo Downloading to temp
        set suffix (if test (lsb_release -is) = 'Fedora'; echo '.rpm'; else; echo '.deb'; end)
        set file (mktemp --suffix $suffix); or begin
            echo 'mktemp fail' >&2; and return
        end
        echo s
        http --check-status -bdo $file $argv; or begin
            echo 'download error' >&2; and return
        end
        echo downloaded
    end
    
    # file ?
    if string match '*.*' $file 
        echo Installing from file: $file
        if test (lsb_release -is) = 'Fedora'
            sudo dnf install $file
        else 
            sudo dpkg -i $file
        end
    end

    # Package 
    if test (lsb_release -is) = 'Fedora'
        sudo dnf install $file
    else 
        sudo apt install $file
    end
end
