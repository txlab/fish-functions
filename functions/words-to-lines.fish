#!/usr/bin/env fish
function words-to-lines --description 'print each input word on a separate line'
    xargs -l -d' ' echo
end
