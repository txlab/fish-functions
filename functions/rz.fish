function rz --wraps='alacritty -T SSH:$REMOTE -e ssh -t $REMOTE' --description 'launch an alacritty window with a remote ssh zellij session'
    if set -q argv[1]
        set REMOTE $argv[1]
    else
        echo 'usage: rz <ssh host> [session name]'
        echo 'rz dev - to use entry for dev from your ssh config with the default session name'
        echo 'rz dev extra - to use entry from your ssh config and make attach/create a session named extra'
        echo 'rz user@host - to connect to user@host via ssh'
        return 1
    end

    echo $argv[2]
    if set -q argv[2]
        set SESSION_NAME "$argv[2] ➤ $REMOTE"
    else
        set SESSION_NAME "$(hostname) ➤ $REMOTE"
    end
    echo "connecting to: $REMOTE  $argv[2] into session: $SESSION_NAME"
    set fish_trace 1
    alacritty -T SSH:$REMOTE -e ssh -t $REMOTE "zellij attach '$SESSION_NAME' || zellij -s  '$SESSION_NAME'" &
end
