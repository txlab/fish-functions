function nix-expand-simple-refs -d 'allows specifying package names without nixpkgs# (but also supports long variant & github urls)'
    for arg in $argv
        if string match -qr '^-' -- "$arg" # args might be options
            echo $arg
        else if string match -qr '^(github(:|.com)|nixpkgs|latest)' "$arg"
            echo $arg
        else if string match -qr / "$arg" # arg might be a path
            echo $arg
        else if string match -qr '#' "$arg" # arg might be flake ref
            echo $arg
        else
            echo nixpkgs#$arg
        end
    end
end
