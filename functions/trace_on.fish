function trace_on --description 'enable fish_trace'
    set -g fish_trace 1
end
