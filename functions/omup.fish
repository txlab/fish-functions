function omup --wraps='omf update && omf reload' --description 'alias omfup omf update && omf reload'
  if string match -qr '^/etc' $OMF_PATH
    sudo fish -c 'omf update'; or return 1
  else
    omf update; or return 1
  end

  omf reload $argv; 
end
