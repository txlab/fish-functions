#!/usr/bin/env fish

function boldecho -d description
    echo (set_color --bold)$argv(set_color normal)
end
