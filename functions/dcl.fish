#!/usr/bin/env fish
# Defined in - @ line 1
function dcl --description 'alias dcl dc logs --tail=30 -f'
	dc logs --tail=30 -f $argv;
end
