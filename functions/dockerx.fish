#!/usr/bin/env fish
# Defined in - @ line 1
function dockerx --description 'alias dockerx docker buildx'
	docker buildx $argv;
end
