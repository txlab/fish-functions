function gpp --wraps='g pull && g push' --description 'alias gpp g pull && g push'
  g pull && g push $argv; 
end
