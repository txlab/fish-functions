#!/usr/bin/env fish
function watchf -d "watch command (in fish shell)" --wraps="watch"
    set cmd_start 1
    if string match -r ' -- ' -- "$argv" > /dev/null
        for i in (seq (count $argv))
            # echo "cmd_start?" $i
            if test $argv[$i] = '--'
                # echo found $argv[$i]
                break
            end
            set -a options $argv[$i]
            set cmd_start (math $i + 2)
        end
    end
    set options (string trim -- $options)
    # echo options: $options
    # echo cmd_start: $cmd_start
    # if test -z "$options"
    set -a options '--'
    # end

    #if test (count $argv[$cmd_start..-1]) -gt 1
    #    echo "Only one argument allowed as command, will be passed to fish -c" 2>&1
    #    return 1
    #end

    # echo watch -x $options fish -c '"'$argv[$cmd_start..-1]'"'
    # env HOME=(mktemp -d)  ...
    printrun watch -cx $options fish -c "$argv[$cmd_start..-1]"
end
