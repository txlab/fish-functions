#!/usr/bin/env fish
# 1. sd (which searches and replaces) - https://github.com/chmln/sd
# 2. delta (displays diff nicely) - https://github.com/dandavison/delta
function sdp --wraps='delta $argv[3] (sd -p $argv | psub)' --description 'sd -p $argv & delta with original file ($argv[3])'
    difft $argv[3] (sd -p $argv | psub)
end
