#!/usr/bin/env fish
# Defined in - @ line 1
function pi --wraps='pnpm' --description 'alias for: pnpm i'
    pnpm i $argv
end
