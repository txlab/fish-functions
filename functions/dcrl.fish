#!/usr/bin/env fish
function dcrl --wraps='dcl' --description 'alias: "dcr; and dcl"'
  dcr; and dcl $argv; 
end
