#!/usr/bin/env fish

function cpe --wraps='cp' --description 'copy & edit resulting file' -a source -a dest
    cp $source $dest; or return $status
    e $dest
end
