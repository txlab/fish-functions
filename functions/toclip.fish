#!/usr/bin/env fish
# Defined in - @ line 1
function toclip --description 'alias toclip xsel --clipboard --input'
	xsel --clipboard --input $argv;
end
