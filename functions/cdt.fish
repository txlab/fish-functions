#!/usr/bin/env fish
function cdt --wraps='cdm (mktemp -d)' --description 'alias cdt cdm (mktemp -d)'
  cdm (mktemp -d) $argv; 
end
