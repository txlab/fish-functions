#!/usr/bin/env fish

function cp-last-output -d "Rerun last command & copy output to clipboard"
    set last_cmd (last-cmd) # ; or return ?
    echo '$' $last_cmd >&2
    if string match -q "*last-cmd*" -- $last_cmd
        echo "Last command contains 'last-cmd' - aborting." # little infinite loop catcher.. BTDT
        exit 2
    end
    # eval (string escape -- $last_cmd) | wl-copy # https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
    set tmp (mktemp); or return
    eval $last_cmd &| tee $tmp
    cat $tmp | wl-copy
end
