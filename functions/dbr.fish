#!/usr/bin/env fish

function dbr -d "Docker build & run"
    set orig_argv $argv # argparse swallows `--`
    # echo orig_argv $argv
    argparse --ignore-unknown 'b/build=' 'f/file=' 't/tag=' -- $argv; or return
    # echo argv $argv
    set -q _flag_tag || set _flag_tag build-run
    set build_flags $_flag_build
    set run_flags
    set container_args
    set after_double_dash 0

    set doubledash_pos (contains -i -- '--' $orig_argv)
    for arg in $argv
        set orig_pos (contains -i -- $arg $orig_argv)
        # echo [$arg] orig $orig_pos doubledash $doubledash_pos
        if test -n "$doubledash_pos" && test $orig_pos -gt $doubledash_pos
            set container_args $container_args $arg
        else
            set run_flags $run_flags $arg
        end
    end

    if set -q _flag_file
        set build_flags -f $_flag_file $build_flags
    end

    docker build $build_flags -t $_flag_tag . || return $status

    dr $run_flags $_flag_tag $container_args
end
