#!/usr/bin/env fish
function rec-gpt --description 'record session & send to ChatGPT'
    set cast (mktemp --tmpdir XXX.cast)
    asciinema rec $cast; or return 1
    read -p "echo 'Prompt: '" prompt
    or return 1

    echo
    asciinema cat $cast | chatgpt -p $prompt
    # chatgpt -p (cat $cast)

    echo
    echo "asciinema cat $cast | chatgpt -p"
    return $status
end
