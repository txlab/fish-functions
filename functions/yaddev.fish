#!/usr/bin/env fish
# Defined in - @ line 1
function yaddev --wraps='yarn add --dev' --description 'alias yaddev yarn add --dev'
  yarn add --dev $argv;
end
