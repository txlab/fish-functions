#!/usr/bin/env fish
function rmr --wraps='rm -rf' --description 'alias for "rm -rf"'
  rm -rf $argv; 
end
