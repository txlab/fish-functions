function deve -d "Edit devenv.nix of current direnv"
    if test -z "$DIRENV_FILE"
        echo "Not in a direnv" >&2; return 2
    end
    set devenv_file (dirname (echo $DIRENV_FILE))/devenv.nix
    if not test -f $devenv_file
        echo "No file: $devenv_file" >&2; return 2
    end
    e $devenv_file
end
