function nds --wraps='nix shell nixpkgs#$argv[1] $argv[2..]' --description 'in docker: nix shell nixpkgs#$argv[1] $argv[2..]'
    printrun dr nixpkgs/nix-flakes nix shell nixpkgs#$argv[1] $argv[2..]
end
