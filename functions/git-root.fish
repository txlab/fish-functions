#!/usr/bin/env fish
function git-root
    git rev-parse --show-toplevel
end
