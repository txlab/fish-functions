function gam --wraps='gc --amend' --description 'alias gam gc --amend'
  gc --amend $argv; 
end
