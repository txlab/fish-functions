#!/usr/bin/env fish
function dcr --wraps='dc restart' --description 'alias dcr dc restart'
  dc restart $argv; 
end
