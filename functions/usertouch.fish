#!/usr/bin/env fish
function usertouch
sudo touch $argv[2..-1]
sudo chown $argv
end
