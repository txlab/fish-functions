#!/usr/bin/env fish
function retry-ndone
	retry eval (string escape -- $argv) # https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
	ndone "retry done"
end
