function pid --wraps='pi -D' --description 'alias pid pi -D'
  pi -D $argv; 
end
