#!/usr/bin/env fish
function in-fresh-env --description 'Run in empty $HOME & without environment variables (but /etc/.bashrc & co)'
    set tmp_home (mktemp -d) || return 1
    echo "Executing with HOME in $tmp_home" >&2

    # eval bc. https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
    eval \
        env -i HOME=$tmp_home \
        (string escape -- $argv)
end
