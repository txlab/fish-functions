function watchlog
    argparse -i 't/timeout=?' h/help -- $argv; or return
    set -q _flag_h || test (count $argv) -eq 0 && begin
        echo 'Usage: watchlog [-t=TIMEOUT] <command> <...args>'
        return
    end

    if set -q _flag_t
        set timeout $_flag_t
    else
        set timeout 10
    end

    while true
        echo (date -Iseconds)
        eval (string escape -- $argv) # https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
        sleep $timeout
        echo
    end
end
