#!/usr/bin/env fish
function dsh --description 'go into a shell for the specified container (tries the folder basename if called with no args)'
    # Service?
    set service $argv[1]
    if test -z "$service"
        set service (basename $PWD)
    end

    # Shell?
    set shel bash
    set has_bash (dc exec $service which $shel)
    if test -z "$shel"
        set shel sh -l
    end

    # dc exec $service $shel
    # detect index via:
    docker exec -it (dc ps --format json | jq -r '.[] | select(.Service == "'"$service"'") | .Name') $shel
end

function __fish_dsh_complete_services
    dc ps --format json 2>/dev/null | jq -r '.[].Service'
end

complete -c dsh -f
complete -c dsh -n "not __fish_seen_subcommand_from (__fish_dsh_complete_services)" -a "(__fish_dsh_complete_services)"
