#!/usr/bin/env fish
function dconf2nix-section --arg path --description 'dconf dump & nixify a section'
    dconf dump $path | dconf2nix -e -r $path
end
