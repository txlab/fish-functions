#!/usr/bin/env fish
function drpwdc --wraps='docker run --rm -it -v $PWD:/app -w /app' --description 'Copy PWD to tempdir and run a docker command in it'
  set tmpdir (mktemp -d); or return $status
  # echo "Copying to $tmpdir" >&2
  printrun rsync -aL $PWD/ $tmpdir/; or return $status
  printrun dr -v $tmpdir:/app -w /app $argv; or return $status
end
