#!/usr/bin/env fish
function lsp -d 'll without time & size'
    ll --time-style=+"" $argv | awk '{$2=""; $5=""; print}'
end
