#!/usr/bin/env fish
# Usage:
#   set -l url (arg_require $argv[1]) || return
#
# (return is needed because in here we can only exit our inner function)

function arg_require -d 'returns arg or exit 1'
    if test -n "$argv"
        printf '%s\n' $argv # if there where multiple arguments they should be on separate lines to be interpreted as a list variable (instead of a string with spaces)
    else
        echo 'Missing required argument' >&2
        return 1
    end
end
