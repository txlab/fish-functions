#!/usr/bin/env fish
function lintgrep
    #for line in (cat lint.log)
    while read line
        echo \> (string length $line) $line
        if test -z $line
            set -e file
            echo ---
            continue
        end
        if test -z $file
            set file (realpath $line --relative-to (pwd)) # make relative
            echo FILE (string length $file) $file
        end
        if string match -e $argv[1] $line
            echo MATCH
            echo $file:(echo $line | sed -E 's|^ +([0-9]+(?::[0-9]+)?))).+|\1|') # add line & col number
        end
    end
    echo "DONE $status"
end
