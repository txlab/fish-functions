#!/usr/bin/env fish
function passe -d 'pass edit via fzf match'
    set query $argv[-1]
    # set opt $argv[1..-2]
    # test -n "$opt"; or set opt "" # by default, copy to clipboard and show extra secrets
    # echo opt=$opt q=$query

    set passname (pass ls --flat | fzf -1 -q $query --preview "pass show {}")
    printrun pass edit $passname
end
