#!/usr/bin/env fish

function repeat-copy -d "Repeat last command and copy output"
    set log (mktemp); or return
    eval (last-cmd) | tee $log
    cat $log | wl-copy
    # and echo Copied.
end
