#!/usr/bin/env fish
function retry
	while not eval (string escape -- $argv) # https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
        echo
        sleep 1
    end
end
