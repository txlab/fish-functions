function cmos --wraps='cmo g+rw :sudo' --description 'Change group to sudo & chmod g+rws (read write and sticky)'
    argparse h/help r/recursive v/verbose -- $argv; or return
    set -q _flag_r && set opts -r

    if test $PWD = /
        echo 'not in the root folder you don\'t' >&2
        return 1
    end

    # defaults:
    if getent group sudo >/dev/null
        set defg sudo
    else
        set defg wheel
    end
    set ug ":$defg"
    set target '.'

    test -n "$argv[2]" && set target "$argv[2..]"

    switch $argv[1]
        case ':*'
            set ug $argv[1]
        case '*:*'
            set ug $argv[1]
        case '*:'
            set ug "$argv[1]$defg"
        case '*?*'
            set target "$argv[1]"
        case '*'
            echo WTF && return 1
    end

    #set -q _flag_v && echo "cmo $opts g+rw $ug $target"
    #echo "cmo $opts g+rws $ug $target"
    printrun cmo $opts g+rw $ug $target

    # only add sticky bit on directories
    if set -q _flag_r
        find $target -type d -print0 | xargs -0 chmod g+xs
    else
        test -d $target && chmod g+s $target
    end

end
