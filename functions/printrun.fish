#!/usr/bin/env fish
function printrun --description 'print command and run it (respecting NO_PRINTRUN env)'
    # set fish_trace_depth 1 - https://github.com/fish-shell/fish-shell/issues/9069
    if not set -q NO_PRINTRUN
        echo '$' $argv >&2
    end
    eval (string escape -- $argv) # https://github.com/fish-shell/fish-shell/issues/3708#issuecomment-271035693
    return $status
end
