# Open closest $file of this parent directory, e.g.:
# $ clo devenv.nix
function clo -a file
    set -l current_dir $PWD
    while test "$current_dir" != "/"
        set -l current_file "$current_dir/$file"
        if test -f $current_file
            e "$current_file"
            return
        end
        set current_dir (dirname $current_dir)
    end
    echo "'$file' not found in any parent directory of $PWD"
    return 1
end
