#!/usr/bin/env fish

function chg-review -d DESCRIPTION -a files
    argparse h/help 'p/prompt=' -- $argv; or return
    if set -q _flag_h #; or begin
        #    not set -q files; and test -t 0 # TODO
        #end
        # set -q files && echo files: $files || echo nofiles $files
        # test -t && echo test-t || echo notest-t

        echo 'Usage: chg-review [-p/--prompt=PROMPT] files...'
        echo -e '\nInstead of files you can also pipe to stdin.'
        return
    end
    if not set -q prompt
        set prompt 'Please code-review this:\n'
    end
    if test -z "$files"
        set files -
    end
    # set --show files

    if test "$files" = -
        cat | read -z content
        echo "$content"
    else
        bat $files
    end

    begin
        echo -e $prompt
        if set -q content
            echo $content
        else
            bat -f $files
        end
    end | chatgpt
end
