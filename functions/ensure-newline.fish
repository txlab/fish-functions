function ensure-newline --description 'ensure file(s) last line is empty'
    for f in $argv
        if not test -f $f
            touch $f; or return 1
            continue
        end
        
        set last_char (tail -c 1 $f); or return 1
        # echo "last char:" "'$last_char'"
        if test "$last_char" = (echo -en "\n")
            # echo ADDING
            echo "" >>.gitignore; or return 1
        end
    end
end
