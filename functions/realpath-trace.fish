#!/usr/bin/env fish

function realpath-trace --argument path recursion -d "trace chain of links and print each step"
    echo $path
    if test -z $recursion
        set recursion 0
    end
    if set target (readlink $path) && test -L $target
        realpath-trace $target (math $recursion + 1)
    else
        echo $target
    end
end
