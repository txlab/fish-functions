function srvrg --wraps='fd -Htf -d2 /srv | xargs rg' --description 'alias srvrg fd -Htf -d2 /srv | xargs rg'
  fd -Htf -d2 . /srv | xargs rg $argv 2> /dev/null
end
