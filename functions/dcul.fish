#!/usr/bin/env fish
function dcul --wraps='dc up -d; and dcl' --description 'alias dcul dc up -d; and dcl'
  dc up -d; and dcl $argv; 
end
