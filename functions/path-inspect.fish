#!/usr/bin/env fish

function path-inspect -d "Show binaries in $PATH entries" -a binary
    for path in (echo $PATH | xargs -l -d ' ' echo | tac)
        if test -n "$binary"
            ls $path 2>/dev/null | grep -Fxq "$binary"
            or continue
            boldecho ">>> $path"
            ls $path | grep -Fx "$binary"
        else
            boldecho ">>> $path"
            ls $path
            echo
        end
    end
end
