#!/usr/bin/env fish
function passf -d 'pass | fzf'
    set cmd (which gopass >/dev/null && echo 'gopass' || echo 'pass')
    # echo CMD: $cmd
    set query $argv[-1]
    set opt $argv[1..-2]
    test -n "$opt"; or set opt "-C" # by default, copy to clipboard and show extra secrets
    # echo opt=$opt q=$query

    $cmd show $opt (
        $cmd ls --flat | fzf -1 -q "$query" --preview "$cmd show {}"
    )
end
