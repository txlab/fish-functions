#!/usr/bin/env fish
function gcdc -a repo -a dir --description 'alias: git clone $argv; and cd in - requires repo url - optional dir to clone into'
    test (count $argv) -eq 0 && begin
        echo 'Usage: gcd repo [target_dir]'
        return
    end

    test (basename (pwd)) != dev && string match -r '^y' (read -P 'cd into ~/dev y/n?') && cd ~/dev || true

    if test -z "$dir"
        set dir (basename "$repo" .git)
    end
    if test -n "$(fd -HI1 . $dir 2>/dev/null | string trim)" # it has files in it
        echo "Directory '$dir' has contents, creating subdirectory"
        set dir $dir/(basename "$repo" .git)
    end

    git clone $repo $dir; or return $status
    cd $dir && codium .
end
