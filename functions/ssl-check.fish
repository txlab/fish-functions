#!/usr/bin/env fish

function ssl-check -d "Get SSL cert domains for domain" -a domain
    set url (arg_require $argv[1]) || return
    openssl s_client -connect $domain:443 -servername $domain </dev/null 2>/dev/null | openssl x509 -noout -ext subjectAltName
end
