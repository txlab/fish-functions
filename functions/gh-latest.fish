#!/usr/bin/env fish
# Download latest GitHub release artifact
# 
# Examples:
# `gh-latest org/repo`   # (single file downloaded to pwd)
# `gh-latest org/repo archive.zip\$ -xo repo_files/`
# `gh-latest org/repo amd64.deb\$ -i`
# `gh-latest org/repo linux.zip\$ -xXo ~/.local/bin/binary`

function gh-latest -d "Download latest github release artifact"
    argparse h/help 'o/output=' i/install 'x/extract' 'X/executable' -- $argv || return
    set -q _flag_h && echo 'Usage: gh-latest repo [matchRegex] [-o/--output=PATH] [-i/--install] [-x/--extract] [-X/--executable]' && return
    set repo (arg_require $argv[1]) || return 1
    set match $argv[2]
    if test -z "$match" && set -q _flag_i  # default value when using -i - which I often used
        set match (test (lsb_release -is) = 'Fedora' && echo 'amd64.rpm$' || echo 'amd64.deb$')
    end
    set output $_flag_o
    set executable $_flag_X
    # set -S # debug print variables

    ## Query API ##
    echo "Querying latest release for https://github.com/$repo"
    set info (http https://api.github.com/repos/$repo/releases/latest) || return $status
    #TODO: add flag to download pre-releases - https://docs.github.com/en/rest/releases/releases#list-releases
    # Filter assets
    if test -n "$match"
        echo "Filtering" (echo $info | jq -r '.assets | .[].name' | count) "assets with '$match'"
        set urls (echo $info | jq -r '.assets | .[] | select(.name|test("'$match'")) | .browser_download_url')
    else
        set urls (echo $info | jq -r '.assets | .[] | .browser_download_url')
        or begin # print API results when jq doesn't find the expected structure (e.g. invalid project)
            echo $info
            return 1
        end
    end

    # Check if single asset matched
    if test (count $urls) -ne 1
        # TODO: interactive selector if multiple matches
        echo "Error: found "(count $urls)" matching:" >&2
        if test (count $urls) -eq 0
                or test -z "$match"
            # No match (or no filter given) - print all names & jq command troubleshooting
            echo $info | jq -r '.assets | .[] | .name' | xargs printf '- %s\n'  >&2
            echo '| select(test("'$match'"))' >&2
        else
            # print matched
            echo $info | jq '.assets | .[] | select(.name|test("'$match'")) | .name' | xargs printf '- %s\n' >&2
        end
        return 1
    end
    set url $urls[1]
    if test -n "$match"
        set name (echo $info | jq -r '.assets | .[] | select(.name|test("'$match'")) | .name')
    else
        echo "Selecting single asset:"
        set name (echo $info | jq -r '.assets | .[] | .name')
    end
    echo "Asset name: $name"

    ## Output filename ##
    # - if one specifies output, make sure the parent directory exists
    if test -n "$output"
        and not test -e (dirname $output)
        echo "Creating parent directory:" (dirname $output)
        mkdir -p (dirname $output)
    end
    # - if one specifies output & extract, the extraction will go to temp and the result to $output
    if test -n "$output"
            and set -q _flag_extract
        echo "Storing output for later:" $output
        set extract_path $output
        set -e output # force downloading to temp
    end
    # - if one does not specify a output, but wants to install or extract, we just download to temp folder
    if test -z "$output"
        if set -q _flag_i 
                or set -q _flag_extract
            set output (mktemp /tmp/XXX_$name) # ... download to tempfile
            echo "Downloading to: $output"
        end
    end

    ## Download ##
    echo # empty line
    echo "Downloading $url"
    http -bd --check-status (test -n "$output" && echo "-o$output") $url || return $status

    ## Extract ##
    if set -q _flag_extract
        echo # empty line

        set extracted (mktemp -d)
        echo "Extracting to '$extracted'"
        if string match -rq '.tar.gz$' "$output"
            printrun tar -xzf "$output" -C "$extracted" || return $status
        else if string match -rq '.tbz$' "$output"
            printrun tar -xjf "$output" -C "$extracted" || return $status
        else if string match -rq '.tar$' "$output"
            printrun tar -xf "$output" -C "$extracted" || return $status
        else
            # default to try zip
            printrun unzip "$output" -d "$extracted" || return $status
        end

        # Is the zip only one file?
        if test (ls -1 "$extracted" | wc -l) -eq 1
            set single_file (ls "$extracted")
            echo "Zip contains only one file: $single_file"
        end
        if set -q single_file
                and set -q extract_path # extract path set, extract it as that
            echo "Moving single file '$single_file' to '$extract_path'"
            mv $extracted/$single_file $extract_path || return $status
            if set -q _flag_executable
                echo "Marking as executable: $extract_path"
                chmod +x $extract_path
            end
        else if test (ls -1 . | wc -l) -eq 0 # Does the working dir contain anything?
                or set -q single_file # Is the zip only one file? - http://unix.stackexchange.com/questions/128303/ddg#128304
                and not set -q extract_path # when there's an out, dont put it here
            echo "Copying here ($PWD)"
            mv $extracted/{,.}* . || return $status # to pwd
        else
            set -q extract_path || set extract_path (basename $name | cut -d. -f1) # output directory is name without extension - https://stackoverflow.com/a/26753382/1633985
            # set extract_path (basename $name | cut -d. -f1) # output directory is name without extension - https://stackoverflow.com/a/26753382/1633985
            echo "Extracting to $extract_path/"
            cp -r $extracted/ $extract_path || return $status
        end
    ## Mark as executable ##
    else if set -q _flag_executable
        if test -z "$output"
            echo "Can't mark as executable as you did not specify output" >&2
        else
            echo "Marking as executable: $output"
            chmod +x $output
        end
    end

    ## Install ##
    if set -q _flag_i
        echo # empty line
        ins $output || return $status
    end
end
