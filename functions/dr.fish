#!/usr/bin/env fish
function dr --wraps='docker run --rm -it' --description 'alias dr docker run --rm -it'
    # echo "$argv"
    argparse -i 'E/entrypoint=?' -- $argv; or return # FIXME: this seems to swallow -e
    # echo "$argv"
    if set -q _flag_E
        set entry (test -z "$_flag_E" && echo 'sh' || echo $_flag_E)
        # echo flag_E? $_flag_E => $entry 
        set argv "--entrypoint=$entry" $argv
    end
    docker run --rm -it $argv
end
