#!/usr/bin/env fish

# list gpg keys for all gpg files (e.g. for pass repo)
function gpg-keys-ls -d "list gpg keys for all gpg files in cwd/$1"
    argparse i/info -- $argv; or return

    set path $argv[1]
    if test -f $path
        set files $path
    else
        set files (fd -e gpg . $path)
    end

    set -l all_keys
    for f in $files
        echo -n $f" "
        set keys_str (gpg --list-packets $f 2>&1 | rg -o 'encrypted with .+ ID ([A-Za-z0-Fx]+)' -r '$1')
        set keys (string split ', ' $keys_str)
        echo $keys

        for key in $keys
            # echo $key
            if not contains $key $all_keys
                set all_keys $all_keys $key
            end
        end
    end

    if set -q _flag_info
        echo
        for key in $all_keys
            echo "Key:" $key
            gpg --list-keys $key | rg uid
        end
    end
end
