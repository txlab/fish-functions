#!/usr/bin/env fish
# Overwritten from /usr/share/fish/functions/la.fish
# we don't like the -A but want -a, to see '.'
# macOS has broke-ass BSD utils https://betterprogramming.pub/how-to-make-macos-command-utilities-compatible-with-gnu-core-utilities-87889b266f4b
function la --wraps=ls --description 'List contents of directory, including hidden files in directory using long format'
    ls -lah -I .. $argv
end
