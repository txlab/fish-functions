#!/usr/bin/env fish
function cc --wraps cd 
    --description 'alias for `cd && clear`'
  cd $argv && clear
end
