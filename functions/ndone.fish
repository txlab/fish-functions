function ndone --wraps='notify-send -u critical -i info' --description 'alias ndone notify-send -u critical -i info'
  notify-send -u critical -i info '[ndone]' $argv; 
end
