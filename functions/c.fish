#!/usr/bin/env fish
function c --wraps=clear --description 'alias for `clear`'
  clear $argv
end
