function npi --wraps='nix profile install' --description 'alias ns nix profile install nixpkgs#$argv[1] [..]'
    printrun nix profile install (nix-expand-simple-refs $argv)
end
