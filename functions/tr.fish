#!/usr/bin/env fish

# complete -c tr -e # HACK: there seem to be conflicting completions
# complete -c tr -w tree 

function tr --wraps tree --description 'tree with sensible defaults'
  tree -L 2 -C --filelimit 100 --dirsfirst --noreport $argv | less -RF
  # TODO: wait for stable debian to include tree v2.0 (https://tracker.debian.org/pkg/tree):
  #  --metafirst --info
end
