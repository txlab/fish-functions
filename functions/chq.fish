#!/usr/bin/env fish

function chq -d DESCRIPTION
    chatgpt -n -p "<pretext>Act as a professional developer\"s AI assistant.
    They have the following preferences:
    - no need for introductory empathy sentences if not important information for the prompt
    - no step by step explanation if a single code output can suffice
    - [if JS question] prefer modern syntax (es6/await)
    Now comes the actual prompt:</pretext>" $argv

    # TODO Copy code-fence to clipboard
end
