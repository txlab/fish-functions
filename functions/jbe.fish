#!/usr/bin/env fish
function jbe -d "jobber edit (root's by default)"
    argparse 'h/help' 'u/user' 'v/verbose' -- $argv; or return
    set -q _flag_h && echo 'Usage: jbe [-u/--user] [-v/--verbose]' && return
    if set -q _flag_u
        test (whoami) = root && echo "don't use -u when root" >&2 && return 2
        test -f ~/.jobber || echo "~/.jobber not initialized, run jobber init" >&2 && return 2
        ed ~/.jobber || return 1
    else
        test -f /root/.jobber || echo "/root/.jobber not initialized, run sudo jobber init" >&2 && return 2
        sued /root/.jobber || return 1
    end

    sudo jobber reload -a | begin
        if not set -q _flag_v
            grep -v "0 jobs"
        else
            cat
        end
    end
    
    echo
    sudo jobber list -a
end
