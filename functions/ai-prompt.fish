function ai-prompt -d "Append input to AI prompt"
    argparse h/help 'i/input=' 'f/file=' -- $argv; or return

    if set -q _flag_h || not set -q _flag_file && test (count $argv) -eq 0
        echo 'Usage: ai-transform [-i/--input=FILE] [-f/--file (to read prompt from)] [prompt]'
        return
    end

    if set -q _flag_file
        set prompt (cat $_flag_file)
    else if test (count $argv) -ge 1
        set prompt $argv[1]
    else
        echo 'Error: You must provide a prompt either as a positional argument or with -f/--file'
        return
    end

    if set -q _flag_input
        set input $_flag_input
    else
        set input (mktemp)
        echo Reading input from stdin...
        cat >$input
    end

    # echo Transforming "$input" # using prompt: $prompt
    # echo
    set temp_output (mktemp)
    begin
        echo $prompt
        echo
        cat $input; or return 1
    end | chatgpt -n | tee $temp_output
    or return $status # FIX: use status of chatgpt command (or any in the pipe)

    echo
    echo "Output file: $temp_output"
end
