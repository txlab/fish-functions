function dig-which-ssh -a service -d 'which of my SSH servers hosts $service (DNS lookup)'
    set service_ips (dig +short $service | string join '|')
    set tmp (mktemp)
    set ssh_hosts (rg 'HostName (.+)' ~/.ssh/config -or '$1' | sort | uniq)
    echo "Checking" (count $ssh_hosts) "ssh hosts"
    for host in $ssh_hosts
        if dig +short $host | rg $service_ips
            echo Match: $host
            return
        end
    end
    echo "Not found: $service_ips"
end
