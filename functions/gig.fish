function gig --description 'add to gitignore (./ => /)'
    ensure-newline .gitignore; or return 1

    for rule in $argv
        # Check if string starts with `./`
        if test (string sub -l 2 -- $rule) = "./"
            # Replace with `/` (this is a hacky way to have path completions, but also add it to gitignore with a leading `/`)
            set rule (string sub -s 2 -- $rule)
        end


        echo "$rule" | tee -a .gitignore; or return 1
    end
end
