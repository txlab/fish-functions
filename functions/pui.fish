#!/usr/bin/env fish
# Defined in - @ line 1
function pui --wraps='pnpm' --description 'interactive update alias for: pnpm up -i --latest'
    pnpm up -i --latest $argv
end
