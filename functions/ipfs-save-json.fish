#!/usr/bin/env fish
function ipfs-save-json --wraps='ipfs block get' --description 'ipfs block get => formatted $cid.json'
	for cid in $argv
		ipfs block get $cid | jq . > $cid.json
		echo $cid.json
	end
end
