#!/usr/bin/env fish

function zellij-run-inplace -d "zellij run --in-place"
    zellij run -i -- fish -lic "z $argv[1]; exec fish $argv[2..]"
end
