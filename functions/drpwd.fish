#!/usr/bin/env fish
function drpwd --wraps='docker run --rm -it -v $PWD:/app -w /app' --description 'alias drpwd docker run --rm -it -v $PWD:/app -w /app'
    dr --mount type=bind,source=$PWD,destination=/app -w /app $argv
end
