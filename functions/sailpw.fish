#!/usr/bin/env fish
# Defined in - @ line 1
function sailpw --description alias\ sailpw\ docker\ logs\ \(docker\ ps\ \|\ tail\ -n+2\ \|\ awk\ \'\{print\ \$1\ \"\ \"\ \$NF\}\'\ \|\ fzf\ \|\ awk\ \'\{print\ \$1\}\'\)\ 2\>/dev/null\ \|\ grep\ -Eo\ \"Password\ is.+\"
	docker logs (docker ps | tail -n+2 | awk '{print $1 " " $NF}' | fzf | awk '{print $1}') 2>/dev/null | grep -Eo "Password is.+" $argv;
end
