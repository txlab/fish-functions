function wait-ssh --wraps='retry ssh $argv echo Pong' --wraps=ssh --description '≈ retry ssh $argv'
  retry ssh $argv echo Pong $argv; 
end
