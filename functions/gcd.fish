#!/usr/bin/env fish
function gcd -a repo -a dir --description 'alias: git clone $argv; and cd in - requires repo url - optional dir to clone into'
    test (count $argv) -eq 0 && begin
        echo 'Usage: gcd repo [target_dir]'
        return
    end

    if test -z "$dir"
        if test -n "$FF_GCD_DEFAULT_DIR"
            set dir $FF_GCD_DEFAULT_DIR
        else
            set dir (basename "$repo" .git)
        end
    end
    if test -n "$(fd -HI1 . $dir 2>/dev/null | string trim)" # it has files in it
        echo "Directory '$dir' has contents, creating subdirectory"
        set dir $dir/(basename "$repo" .git)
    end

    git clone $repo $dir; or return $status
    cd $dir
end
