#!/usr/bin/env fish
function inspect-pipe -a name -d 'both print pipe contents to stderr & forward to stdout'
    if test -n "$name"
        echo -n "$name: " >&2
    end
    
    mkfifo /tmp/fish_stdout   # Create a named pipe
    cat /tmp/fish_stdout >&2 &   # Start a background job that reads from pipe and writes to stderr
    cat | tee /tmp/fish_stdout   # Pipe stdout to both the named pipe and stdout
    rm /tmp/fish_stdout   # Clean up the named pipe.
end
