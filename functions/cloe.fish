# Open closest $file of any parent directory, e.g.:
# $ cloe Taskfile.yml
function cloe -a file
    set f (clo $file)
    or begin
        echo $f
        return 1
    end
    e $f
end
