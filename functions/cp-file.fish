#!/usr/bin/env fish

function cp-file -d "Copy contents of a file to clipboard" -a file
    begin
        if test -n "$file"
            cat $file
        else
            cat -
        end
    end | wl-copy

    set -l check "$(wl-paste)"
    echo Copied (echo "$check" | wc -l) lines.
    echo
    echo '$ head -3:'
    echo "$check" | head -3
end
