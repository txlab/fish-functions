function conf --wraps='dconf watch' --description 'alias dconf watch - add path as an arg without leading /'
  dconf watch "/$argv" | sed 's/\(  \)\(.*\)/=\2;};/;s/\(\/\)\(.*\)\/\(.*\)/"\2"={"\3"/'
end
