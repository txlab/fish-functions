#!/usr/bin/env fish
# Defined in - @ line 1
function dc --wraps='docker-compose' --description 'alias for: docker-compose'
    if command -q docker-compose
        docker-compose $argv
    else
        docker compose $argv
    end
end
