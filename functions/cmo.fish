#!/usr/bin/env fish
# TODO: completions - https://fishshell.com/docs/current/completions.html#completion-own
function cmo -d "chown && chmod (sudo if necessary)"
    argparse h/help r/recursive s/sudo R/ref= -- $argv; or return
    set -q _flag_h || test (count $argv) -eq 0 && begin
        echo -e \
            'Usage: cmo [-r/--recursive] [-s/--sudo] <chmod> <chown> <...files>\n' \
            '  use \'_\' for mod/own args to leave unchanged\n' \
            '  with a single file argument, chown is optional: cmo +x file\n' \
            'Or use reference file for chmod & chown:\n' \
            '  cmo --ref=reference-file.txt target-file'
        return
    end
    set opts -c # default options: -c = display changes made

    if set -q _flag_ref
        echo Using reference file:
        ls -l $_flag_ref || return
        set -a opts "--reference=$_flag_ref"
        set files $argv
    else
        set mod (arg_require $argv[1]) || return
        set own (arg_require $argv[2]) || return
        if test -n "$argv[3..-1]" # do we have at least 3 arguments?
            set files $argv[3..-1]
        else # otherwise we have the shorthand usage (= skip chown arg)
            set files $own
            set -e own
        end
    end

    # echo "opt=" $opts "--" $argv

    if set -q _flag_r
        set -a opts -R
    end
    set user (set -q _flag_s && echo root || whoami) # by default, run as current user (sudo -u (whoami) is kinda pointless but allows to easily run as root with a change of variable)

    if test "$mod" != _
        sudo -u $user chmod $opts $mod $files # if $mod is not set, it will just be skipped
        or test $user != root && begin # if we're not already root, retry as sudo
            set user root
            echo -e "\nRetrying with sudo..."
            sudo -u $user chmod $opts $mod $files
            or return $status
        end
    end

    if set -q own && test "$own" != _ || set -q _flag_ref
        sudo -u $user chown $opts $own $files # if $own is not set, it will just be skipped
        or test $user != root && begin # if we're not already root, retry as sudo
            echo -e "\nRetrying with sudo..."
            sudo chown $opts $own $files
            or return $status
        end
    end

    echo # print final result
    if set -q _flag_r
        sudo -u $user fish -C "set fish_function_path $fish_function_path" -c "tra $files --noreport" # fish -c because of https://github.com/fish-shell/fish-shell/issues/4710#issuecomment-454714172
    else
        sudo -u $user fish -C "set fish_function_path $fish_function_path" -c "lsp -a $files | grep -Ev '^total'" # fish -c because of https://github.com/fish-shell/fish-shell/issues/4710#issuecomment-454714172
    end
end
