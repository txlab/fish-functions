#!/usr/bin/env fish

function cp-last-cmd -d "Copy last command to clipboard"
    set last_cmd (last-cmd)
    echo $last_cmd | wl-copy
    echo $last_cmd
end
