#!/usr/bin/env fish

function wex -d "watchexec with good default flags"
    watchexec -rcclear $argv
end
