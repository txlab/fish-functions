#!/usr/bin/env fish
function ipfs-dag-resolve --wraps='ipfs block get' -a cid -a path --description 'ipfs block get => lookup $path'
	ipfs block get $cid | jq -r .$path
end
