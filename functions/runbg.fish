#!/usr/bin/env fish
function runbg --wraps='nohup' --description 'nohup, no output, & background'
    nohup (string escape -- $argv) 2>&1 >/dev/null &
    disown
end
