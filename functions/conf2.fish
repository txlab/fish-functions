#!/usr/bin/env fish
function conf2 --description 'watch dconf & convert 2 nix'
    
    # HACK to work around dconf2nix choking on utf8 chars - https://github.com/gvolpe/dconf2nix/issues/77
    dconf write /org/gnome/Characters/recent-characters "'[]'"
    dconf write /org/gnome/evolution-data-server/calendar/reminders-past "'[]'"
    
    dconf dump / > /tmp/dconf-baseline.conf || return 1
    dconf2nix -i /tmp/dconf-baseline.conf -e -o /tmp/dconf2nix-baseline.nix > /dev/null || return 1
	echo Saved baseline: /tmp/dconf2nix-baseline.nix

	# TODO: display diff command on Ctrl-C - blocked by https://github.com/fish-shell/fish-shell/issues/6649#issuecomment-1198951287
	# function cancel_func --on-signal SIGINT 
    #     echo "Full diff:"
	# 	echo $VISUAL_DIFF /tmp/dconf2nix-baseline.nix $snap
	# 	functions -e cancel_func
    #     return 0
    # end

	# Cleanup
	rm /tmp/dconf2nix-lastsnapshot.nix >/dev/null

	echo Watching...
    while true
        dconf dump / > /tmp/dconf-snapshot.conf || return 1
        dconf2nix -i /tmp/dconf-snapshot.conf -e -o /tmp/dconf2nix-snapshot.nix > /dev/null || return 1
        if test -f /tmp/dconf2nix-lastsnapshot.nix
            and not diff /tmp/dconf2nix-snapshot.nix /tmp/dconf2nix-lastsnapshot.nix &>/dev/null

            echo # newline
            delta --paging=never --file-style=omit --hunk-header-style=omit -s /tmp/dconf2nix-lastsnapshot.nix /tmp/dconf2nix-snapshot.nix

			set snap /tmp/dconf2nix-snapshot-(date -Is).nix
			cp /tmp/dconf2nix-snapshot.nix $snap
			echo ➥ $snap
			echo $VISUAL_DIFF /tmp/dconf2nix-baseline.nix $snap
        end
        cp /tmp/dconf2nix-snapshot.nix /tmp/dconf2nix-lastsnapshot.nix
		sleep 1
    end

end
