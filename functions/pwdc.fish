#!/usr/bin/env fish
function pwdc --wraps='rsync' --description 'Copy PWD to tempdir and cd into it'
    set tmpdir (mktemp -d); or return $status
    # echo "Copying to $tmpdir" >&2
    printrun rsync -aL $PWD/ $tmpdir/; or return $status
    cd $tmpdir
end
