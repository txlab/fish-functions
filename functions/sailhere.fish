#!/usr/bin/env fish
# Defined in - @ line 1
function sailhere --description 'alias sailhere sail run .; and sleep 1; and docker logs --tail=5 (docker ps -lq)'
	sail run .; and sleep 1; and docker logs --tail=5 (docker ps -lq) $argv;
end
