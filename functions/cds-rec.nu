#!/usr/bin/env nu

let atuin_dir = $env.HOME | path join "dev/sessions/_atuin/"
mkdir $atuin_dir

def main [
    name?: string,
    --noclip(-C),
] {
    let date_str = (date now | format date "%Y-%m-%d_%T")

    # RUN #
    let session_dir = fish -c $"cds ($name | default '') && pwd" 
    # print $session_dir
    cd $session_dir
    let session_name = $session_dir | path basename
    let file_path = $env.HOME | path join "dev/sessions/_asciinema/" $"($session_name).cast"
    # let file_path = $env.HOME | path join "dev/sessions/_asciinema/" $"($name_or_default)_($date_str).cast"
    let atuin_path = $atuin_dir | path join $"($session_name)_($date_str).jsonl"
    
    asciinema rec $file_path --command $env.SHELL

    # Store data #
    let atuin_data = atuin search -c $session_dir --format '"command": "{command}", "directory": "{directory}", "duration": "{duration}", "user": "{user}", "host": "{host}", "time": "{time}", "exit": "{exit}", "relativetime": "{relativetime}"' 
        | str trim | each {|s| '{' + $s + '}'} # HACK json format
    # print ($atuin_data | debug) => $atuin_path
    $atuin_data | save $atuin_path
    
    if not $noclip {
        $file_path | wl-copy
        print "Copied castfile path to clipboard."
    }
}
