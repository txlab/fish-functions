#!/usr/bin/env fish
function fdrg -a find -a grep -d 'find $argv[1] | xargs rg $argv[2]'
    fd $find $argv[3..] -X rg "$grep" {}
    # TODO: allow arguments for both commands (based on them being before or after positional args)find and ripgrep on the results
end
