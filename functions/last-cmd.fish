#!/usr/bin/env fish

function last-cmd -d "Echo last command"
    history --max=1 \
        | string collect -N #\
        # | string split -f 2- -n " "
end
