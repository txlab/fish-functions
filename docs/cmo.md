# `cmo`
shorthand for **chmod** & **chown** (which automatically retries with sudo if it doesn't succeed)

### Options
**`-s/--sudo`** directly use sudo  
**`-r/--recursive`** recursive  
**`-Rfile/--ref=file`** use `file` as reference (Copy ownership & permissions)  

### Examples
```terminal
$ cmo 700 user:group file1 file2

$ cmo g+r user file1 file2

# skip chmod and only do chown
$ cmo _ :group file1 file2

# skip chown (chown='_' for can be omitted if only single file)
$ cmo g+rx _ file1 file2
# shorthand if only single file:
$ cmo g+rx file

# Use reference file:
$ cmo --ref=otherfile file
$ cmo -Rotherfile file
```
