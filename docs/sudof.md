# `sudof`
Run a command as sudo but with fish functions available

```
$ sudof tra /tmp/fo
[drwx------ root     root        60]  /tmp/fo
└── [-rw—-—-- root     root         0]  x
```
